# WST Mini-Project

This is the wst mini-project based on the __rt-camp__ web-developer hiring [assignments](https://github.com/rtCamp/hiring-assignments/tree/master/Web-Developer).  

Following is the list of goals implemented:

#### Part-1: Album Slideshow

1. User visits your script page
2. User will be asked to connect using his FB account
3. Once authenticated, your script will pull his album list from FB
4. User will click on an album name/thumbnail
5. A pure CSS and Plain JS slideshow will start showing photos in that album (in full-screen mode)


#### Part-2: Download Album

1. Besides every album icon (step #4 in part-1), add a new icon/button saying "Download This Album"
2. When the user clicks on that button, your script will fetch all photos in that album behind the scene and zip them inside a folder on server.
3. You may start a "progress bar" as soon as user-click download button as download process may take time.
4. Once zip building on server completes, show user link to zip file.
5. When user clicks zip-file link, it will download zip folder without opening any new page.
6. Beside album names, add a checkbox. Then add a common "Download Selected Album" button. This button will download selected albums into a common zip that will contain one folder for each album. Folder-name will be album-name.
7. Also, add a big "__Download All__" button. This button will download all albums in a zip, similar to above.

#### Part-3: Backup albums to Google Drive
1. Provide the user with an option to move albums to a Google Drive.
The Google Drive will contain a master folder whose name will be of the format ```facebook_<username>_albums``` where username will be the Facebook ```username``` of the user.
The user's Facebook albums will be backed up in this master folder. Photos from each album will go inside their respective folders. Folder names will be the same as the Facebook album names.
2. To improve the user experience, include the three following buttons:
    "Move" button- This button will appear under each album on your website. When clicked, _the corresponding album only_ will be moved to Google Drive
    "Move Selected"- This button will work along with a checkbox system. The user can select a few albums via checkboxes and click on this button. Only the _selected albums_ will be moved to Google Drive
    "Move All"- This button will immediately move _all user albums_ to Google Drive within their respective folders.
3. Make sure that the user is asked to connect to their Google account only once, no matter how many times they choose to move data.


## Installation

To run the application download the entire repo and navigate to the folder. Then run a php server using the following command 
```bash
php -S 127.0.0.1:4000
```

**Note** - Only use the _port 4000_ as it has been set on the Facebook developer site as the callback URL. To use another port the developer needs to make changes in the App Settings on [Facebook](https://developer.facebook.com/apps) and [Google](https://developer.google.com/drive) in __fbconfig.php__ and __gpConfig.php__ file respectively.
## Usage

You can set up your own app on [Facebook Developer](http://developers.facebook.com) and [Google Developer](http://developers.google.com) and then run the application with the **Callback URL** of your choice.  
If you choose to do so then you will also need to change the __fbconfig.php__ and __gpConfig.php__ file. Set the *app_id*, *app_secret* and *callback* parameters with the new keys that you will get in your [Facebook App Settings](https://developers.facebook.com//apps) and the *client_Id*, *clientSecret* and *redirectURL* parameters with the new keys that you will get in your [Google App Settings](https://developers.google.com/drive).

## References and Libraries 
1. rtCamp - [https://github.com/rtCamp/hiring-assignments/tree/master/Web-Developer](https://github.com/rtCamp/hiring-assignments/tree/master/Web-Developer)
2. Facebook - [https://developers.facebook.com/en/docs](https://developers.facebook.com/docs/)  
3. Google Drive - [https://developers.google.com/drive/](https://developers.google.com/drive/)
4. Bootstrap - [https://getbootstrap.com/docs/3.3/getting-started/](https://getbootstrap.com/docs/3.3/getting-started/)
5. Jquery - [https://code.jquery.com/](https://code.jquery.com/)
6. AngularJS - [https://angularjs.org/](https://angularjs.org/)

## Submitted by
[Atharva Jadhav - 111608031](https://gitlab.com/atharvaa)
# WST rtcamp Facebook challenge

A Facebook based Web Application where user can login with facebook and 
check out their albums and download as a ZIP File them and
if they wish they can login through google account and
store album's in Google-Drive.