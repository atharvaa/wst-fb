<?php
include "fbconfig.php";

/* 	Gets the Album ID to be displayed.
 * 	Checks whether Access token is correct.
 * 	Gets the source of all the photos in that album
 *  Stores them in useralbumimages as array
 *  Uses baguette box to show the slideshow
 */
if (isset($_GET['useralbumid'])) {
	if (isset($_SESSION['facebook_access_token'])) {
		$fb_obj->setDefaultAccessToken($_SESSION['facebook_access_token']);
		try {
			$profile_request = $fb_obj->get('/me?fields=picture.width(200).height(200),id,name,cover');
			$profile = $profile_request->getGraphNode()->asArray();
			$useralbumimage_response = $fb_obj->get("/" . $_GET['useralbumid'] . "/photos?fields=source,name,id");
			$useralbumimages = $useralbumimage_response->getGraphEdge()->asArray();
		} catch (Facebook\Exceptions\FacebookResponseException $e) {
			// When Graph returns an error
			echo 'Graph returned an error: ' . $e->getMessage();
			// redirecting user back to app login page 
			header("Location: ./");
			exit;   
		} catch (Facebook\Exceptions\FacebookSDKException $e) {
			// When validation fails or other local issues
			echo 'Facebook SDK returned an error: ' . $e->getMessage();
			exit;
		}
	} else {
		header("location:localhost:4000/");
	}
} else {
	header("location:./");
}
?>

<!DOCTYPE html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Facebook</title>

		<link rel="stylesheet" type="text/css" href="lib/css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="lib/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="lib/css/font-awesome.css">
		<link rel="stylesheet" type="text/css" href="lib/css/font-awesome.min.css">

		<link rel="stylesheet" type="text/css" href="lib/css/profile.css">
		<link rel="stylesheet" type="text/css" href="lib/css/gallery-grid.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.css">
</head>
<body>
<nav class="navbar navbar-default navbar-fixed-top" style="color: #e9ebee;">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		<a href="#" class="fa fa-facebook-official" href="fb-callback.php" style="margin-top: 22px;font-size:36px; color:white"></a>
		</div>
		<div class="collapse navbar-collapse" id="myNavbar">
			<ul class="nav navbar-nav navbar-right">
				<li><a class="navbar-toggle" href="logout.php" style="margin-top: 4px;"><h5 style="font-family: sans-serif;">Log Out</h5></a></li>
				<li></li>

			</ul>
		</div>
	</div>
</nav>

<div class="container gallery-container" id="gallery" >
	<div class="text-center"><h2 class="heading_h2" style="margin-top: 50px;color: #4b4f56;">Photos</h2></div>
		<div class="tz-gallery">
			<div class="row"><?php
			foreach ($useralbumimages as $useralbumimage) {
				echo '<div class="col-sm-6 col-md-4 card-img-top img-responsive">
					<a class="lightbox" href="' . $useralbumimage['source'] . '">
				<img src="' . $useralbumimage['source'] . '" height="300px" width="300px" alt="Park"></a></div>';
			}?>
		</div>
	</div>
</div>

<!-- Baguette box js used for displaying a slideshow -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
<script>
	baguetteBox.run('.tz-gallery');
</script>

<script type="text/javascript" src="lib/js/jquery.min.js"></script>

<!--BOOTSTRAP JS INCLUDES-->
<script type="text/javascript" src="lib/js/bootstrap.js"></script>
</body>
</html>